[
    {
        "label": "学习笔记",
        "title": "学习笔记",
        "badgeProps": {},
        "items": [
            {
                "label": "mysql常用工具",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/Mysql常用工具.md"
            },
            {
                "label": "mysql分区表",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/mysql分区表.md"
            },
            {
                "label": "mysql索引",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/mysql索引.md"
            },
            {
                "label": "事务和锁",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/事务和锁.md"
            },
            {
                "label": "复制",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/复制.md"
            },
            {
                "label": "存储引擎",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/存储引擎.md"
            },
            {
                "label": "视图",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/数据库/视图.md"
            },
            {
                "label": "消息队列",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/消息队列/消息队列.md"
            },
            {
                "label": "maven plugin开发方法",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/java相关/maven插件开发方法.md"
            },
            {
                "label": "springboot动态加载bean",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/java相关/spring-boot动态加载bean.md"
            },
            {
                "label": "mybatis源码阅读一",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/mybatis/mybatis源码阅读一（调试准备）.md"
            },
            {
                "label": "mybatis源码阅读二",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/mybatis/mybatis源码阅读二（构造SqlSessionFactory）.md"
            },
            {
                "label": "redis-相关知识介绍",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/redis/相关知识介绍.md"
            },
            {
                "label": "redis-数据结构",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/redis/redis数据结构.md"
            },
            {
                "label": "redis-jedis常用操作",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/redis/jedis常用操作.md"
            },
            {
                "label": "redis事务和锁",
                "author":"jin",
                "origin_url":"https://github.com/JINZHANG2017/pic-compress.git",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/redis/redis事务.md"
            }
        ]
    },
    {
        "label": "面试问题",
        "title": "Java面试(内容来自github)",
        "badgeProps": {},
        "items": [
            {
                "label": "作者说明",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/README.md"
            },
            {
                "label": "active mq问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/ActiveMQ%E9%97%AE%E9%A2%98.md"
            },
            {
                "label": "Docker问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Docker问题.md"
            },
            {
                "label": "java基础问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Java基础问题.md"
            },
            {
                "label": "Linux问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Linux问题.md"
            },
            {
                "label": "MyBatis问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/MyBatis问题.md"
            },
            {
                "label": "MYSQL问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/MYSQL问题.md"
            },
            {
                "label": "Nginx问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Nginx问题.md"
            },
            {
                "label": "Redis问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Redis问题.md"
            },
            {
                "label": "Spring Cloud Alibaba",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Spring%20Cloud%20Alibaba.md"
            },
            {
                "label": "Spring Cloud 组件相关问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Spring%20Cloud%20%E7%BB%84%E4%BB%B6%E7%9B%B8%E5%85%B3%E9%97%AE%E9%A2%98.md"
            },
            {
                "label": "Spring框架相关问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/Spring框架相关问题.md"
            },
            {
                "label": "zookeeper问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/zookeeper问题.md"
            },
            {
                "label": "商城常见面试题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/商城常见面试题.md"
            },
            {
                "label": "大数据问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/大数据问题.md"
            },
            {
                "label": "操作系统问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/操作系统问题.md"
            },
            {
                "label": "秒杀项目常见面试题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/秒杀项目常见面试题.md"
            },
            {
                "label": "计算机网络问题",
                "author":"lokles",
                "origin_url":"https://gitee.com/lursiaong/Web-Development-Interview-With-Java",
                "image": "https://avatars.githubusercontent.com/u/38750852?s=40&v=4",
                "md_url":"https://gitee.com/jin2022/Web-Development-Interview-With-Java/raw/main/计算机网络问题.md"
            }
        ]
    },
    {
        "label": "照片集",
        "title": "照片集",
        "badgeProps": {},
        "items": [
            {
                "label": "2023-07-15东湖",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/照片合集/2023-07-15.md"
            }
        ]
    },
    {
        "label": "关于应用",
        "title": "关于应用",
        "badgeProps": {},
        "items": [
            {
                "label": "更新日志",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/关于应用/更新日志.md"
            },
            {
                "label": "关于",
                "author":"jin",
                "origin_url":"https://gitlab.com/ZhangJin2020/learndata",
                "image": "https://avatars.githubusercontent.com/u/32434076?v=4",
                "md_url":"https://gitlab.com/ZhangJin2020/learndata/-/raw/master/md/关于应用/关于.md"
            }
        ]
    }
]