# mybatis源码阅读一：调试准备
## 代码目录
从github下载mybatis源代码，使用idea打开目录如下（3.3.0-SNAPSHOT版本)：
![code_struct.png](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/mybatis/code_struct.png)
## 创建测试文件
在src/test文件夹下创建自定义测试用例
![test_struct.png](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/mybatis/test_struct.png)
其中，Main.java文件内容如下：
```
public class Main {
    public static void main(String[] args) {
        String resource="resources/mybatis-config.xml";
        InputStream inputStream=null;
        try {
            inputStream = Resources.getResourceAsStream(resource);//加载配置文件
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);//构造SqlSeesionFactory
        SqlSession sqlSession=null;
        try {
            sqlSession=sqlSessionFactory.openSession();//开启session
            RoleMapper roleMapper=sqlSession.getMapper(RoleMapper.class);//获取Mapper
            Role role=roleMapper.getRole(1L);
            role.setId(2L);
            roleMapper.insertRole(role);
            System.out.println(role.getId()+":"+role.getRoleName()+":"+role.getNote());
            sqlSession.commit();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            sqlSession.rollback();
            e.printStackTrace();
        }finally {
            sqlSession.close();
        }
    }
}
```
包含获取配置，构造SqlFactory，SqlSession等对象，进行基本的select，insert，update操作。
配置文件```mybatis-config.xml```：
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <settings>
        <setting name="lazyLoadingEnabled" value="true"/>
        <!-- <setting name="aggressiveLazyLoading" value="false"/> -->
    </settings>
    <typeAliases>
        <typeAlias alias="role" type="com.gethin.pojo.Role"/>
    </typeAliases>
    <typeHandlers>
        <typeHandler jdbcType="VARCHAR" javaType="string" handler="com.gethin.handler.MyStringHandler"/>
    </typeHandlers>
    <!-- 定义数据库的信息，默认使用development数据库构建环境 -->
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC" />
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver" />
                <property name="url" value="jdbc:mysql://localhost:3307/wfw?useSSL=false" />
                <property name="username" value="root" />
                <property name="password" value="123456" />
            </dataSource>
        </environment>
    </environments>
    <!-- 定义映射器 -->
    <mappers>
        <package name="com.gethin.mapper"/>
    </mappers>
</configuration>
```
RoleMapper.java：
```
package com.gethin.mapper;

import com.gethin.pojo.Role;

public interface RoleMapper {
    Role getRole(Long id);
    Role findRole(String roleName);
    int deleteRole(Long id);
    int insertRole(Role role);
}
```
RoleMapper.xml:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.gethin.mapper.RoleMapper">
    <resultMap type="role" id="roleMap">
        <id column="id" property="id" javaType="long" jdbcType="BIGINT" />
        <result column="role_name" property="roleName" javaType="string"
                jdbcType="VARCHAR" />
        <result column="note" property="note"
                typeHandler="com.gethin.handler.MyStringHandler" />
    </resultMap>
    <select id="getRole" parameterType="long" resultMap="roleMap">
        select
        id,role_name as roleName,note from role where id=#{id}
    </select>
    <select id="findRole" parameterType="long" resultMap="roleMap">
        select
        id,role_name,note from role where role_name like CONCAT('%',#{roleName
        javaType=string,
        jdbcType=VARCHAR,typeHandler=com.gethin.handler.MyStringHandler},'%')
    </select>
    <insert id="insertRole" parameterType="role">
        insert into
        role(role_name,note) value(#{roleName},#{note})
    </insert>
    <delete id="deleteRole" parameterType="long">
        delete from role where
        id=#{id}
    </delete>
</mapper>
```
MyStringHandler.java:
```
package com.gethin.handler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes({String.class})
@MappedJdbcTypes(JdbcType.VARCHAR)
public class MyStringHandler implements TypeHandler<String> {
    private Logger log=Logger.getLogger(MyStringHandler.class);

    @Override
    public String getResult(ResultSet rs, String colName) throws SQLException {
        log.info("使用我的TypeHandler,ResultSet列名获取字符串");
        return rs.getString(colName);
    }

    @Override
    public String getResult(ResultSet rs, int index) throws SQLException {
        log.info("使用我的TypeHandler,ResultSet下标获取字符串");
        return rs.getString(index);
    }

    @Override
    public String getResult(CallableStatement cs, int index) throws SQLException {
        log.info("使用我的TypeHandler,CallableStatement下标获取字符串");
        return cs.getString(index);
    }

    @Override
    public void setParameter(PreparedStatement ps, int index, String value, JdbcType arg3) throws SQLException {
        log.info("使用我的TypeHandler");
        ps.setString(index, value);
    }

}
```