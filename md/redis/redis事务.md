# redis事务
multi命令开启事务，类似mysql中的start transaction
之后执行set命令会返回QUEUED,表示放入队列
exec命令执行事务。会返回多个OK（和队列命令数量一样）
discard放弃执行，类似rollback

- 组队时任何命令出错，组队失败
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/discard.jpg "照片")
- 执行时某条命令出错，只出错命令不执行，其他命令正常执行。
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/discard2.jpg "照片")

- redis乐观锁的实现
watch命令，执行multi之前，先执行watch来监视一个key或多个key，如果在事务执行之前这些key被其他命令改动，事务将被打断，实例：
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/watch1.jpg "照片")
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/watch2.jpg "照片")
watch命令后可以执行unwatch命令来取消监视