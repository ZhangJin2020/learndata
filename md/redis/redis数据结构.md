# redis数据结构
redis中有一个核心对象叫redisObject，用来表示所有的key和value的，用redisObject结构体来表示String，Hash，List，Set，Zset五种数据类型。
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/redisdata.png "照片")
type表示数据类型，及通常说的redis数据类型，encoding表示数据的存储方式，也就是底层数据结构
类型     | 编码|对象
-------- | -----|---
REDIS_STRING  | REDIS_ENCODING_INT| 使用整数值实现的字符串对象
REDIS_STRING  | REDIS_ENCODING_EMBSTR| 使用embstr编码的简单动态字符串实现的字符串对象
REDIS_STRING  | REDIS_ENCODING_RAW| 使用简单动态字符串实现的字符串对象
REDIS_LIST  |  REDIS_ENCODING_ZIPLIST| 使用压缩列表实现的对象列表
REDIS_LIST | REDIS_ENCODING_LINKEDLIST| 使用双端链表实现的对象列表
REDIS_HASH | REDIS_ENCODING_ZIPLIST| 使用压缩列表实现的哈希对象
REDIS_HASH | REDIS_ENCODING_HT| 使用字典实现的哈希对象
REDIS_SET | REDIS_ENCODING_INTSET| 使用整数集合实现的集合对象
REDIS_SET | REDIS_ENCODING_HT| 使用字典实现的集合对象
REDIS_ZSET | REDIS_ENCODING_ZIPLIST| 使用压缩列表实现的有序集合列表
REDIS_ZSET | REDIS_ENCODING_SKIPLIST| 使用跳跃表和字典实现的有序集合列表

命令**object encoding [key]** 可以查看key的存储类型，如：
set key 234;
object encoding key; //返回int
set key2 3.200;
object encoding key2; //返回embstr
## String类型
String类型有三种存储方式，int，raw和embstr
- int 存储整数型值，redisObject中的ptr属性中会保存该值
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/redis-string-int.png "照片")
- SDS(Simple Dynamic String)
    - 存储的字符串是长度大于32个字节的字符串，就会使用SDS存储，并且encoding设置为raw，如果长度小于或等于32字节就会将encoding设置为embstr。
    - 在redis源码中有三个属性，int len,int free, char buf[].
    - len保存了字符串长度，free保存buf数组中未使用的字节数量，buf数组保存具体字符元素。如保存字符串Hello时，sds存储结构如下图所示：
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/redis-string-sds.png "照片")
    - sds比起c语言字符串的优势：(1) sds记录了字符串长度，取长度时间复杂度o(1) (2) c语言中两个字符串拼接，如果没有分配足够长度内存空间就会出现缓冲区溢出的情况，sds会先根据len属性判断空间是否满足要求，如果不够会先进行扩展，因此不会出现溢出的情况 (3) sds提供空间预分配和惰性空间释放两种策略，分配空间时分配的比实际要多，减少连续执行字符串增长带来的内存重新分配的次数，预分配原则是当修改后字符串长度len小于1MB时会分配和len一样长度的空间，大于等于1MB时会分配1MB空间
    - sds是二进制安全的，可以存储图片等文件，而c语言以空字符作为结束符，不能保证二进制安全。
## HASH类型
hash类型实现方式有两种分别是ziplist，hashtable。
- hashtable原理可以和java中的hashmap类比
- ziplist
    - 压缩列表是一组连续内存块组成的顺序的数据结构，能节省空间。内存结构图：
    ![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/ziplist.png "照片")
    - zlbytes: 4字节，记录压缩列表占用的总字节数
    - zltail：4字节，记录表尾节点离起始地址的偏移量，用于快速定位表尾节点。
    - zllen：2字节，记录压缩列表中的节点数
    - entry：表示每个节点，由三部分组成，包括previous_entry_ength(表示前一个节点的长度，用于计算前一个节点的真实地址)，encoding（保存的是content内容类型和长度），content：（保存每个节点的实际内容）
    - zlend：压缩列表结束符'0xFF'
    - hash应用场景： 
        - 存储对象型数据，相比string类型没有序列号性能开销
        - 分布式生成唯一ID
```
    // offset表示的是id的递增梯度值
    public Long getId(String key,String hashKey,Long offset) throws BusinessException{
        try {
            if (null == offset) {
                offset=1L;
            }
            // 生成唯一id
            return redisUtil.increment(key, hashKey, offset);
        } catch (Exception e) {
            //若是出现异常就是用uuid来生成唯一的id值
            int randNo=UUID.randomUUID().toString().hashCode();
            if (randNo < 0) {
                randNo=-randNo;
            }
            return Long.valueOf(String.format("%16d", randNo));
        }
    }
```
## List类型
- redis 3.2版本之前用的是ziplist和linkedlist来实现的，之后版本就引入了quicklist
- linkedlist是一个双向链表。
- quicklist实际上是zipList和linkedList的混合体，它将linkedlist按段切分，每一段使用ziplist来紧凑存储，多个ziplist之间使用双向链表连接起来
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/quicklist.png "照片")
- redis中链表的特性：
    - 每一个节点都有一个指向前一个节点和后一个节点的指针
    - 头节点和尾节点的pre和next指针指向为null，所以链表无环
    - 链表保存了长度信息，所以获取长度的时间复杂度是o(1)
- 应用场景
    - 列表可以实现**阻塞队列**，结合lpush和brpop命令可以实现。
    - redis实现阻塞队列的方式：https://blog.csdn.net/lin_keys/article/details/106150044

## Set类型
- 底层是ht(哈希表)和intset
- intset整数集合，用于保存整数值的数据结构
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/redis-inset.png "照片")
- 有三个属性值encoding,length,contents,分别为编码方式，整数集合长度，以及元素内容，length就是contents里面的大小。
- 整数集合新增元素可能会对集合进行升级，具体升级过程：
    - 扩展底层数组大小，如果新的编码格式大于现在的编码格式，则升级并添加元素(新增需要的位空间，从后往前调整数据)
    - 数组升级后编码会一直保存升级后的状态，不支持降级。
- 使用inset数据结构需要满足下述两个条件：
    - 元素个数不少于默认值512
    - 元素可以用整型表示
- 应用场景： 去重，抽奖，共同好友等
## Zset类型
- 底层实现是字典(dict)+跳表(skiplist)，满足两个条件使用ziplist：
    - 有序集合保存的元素数量小于默认值128个
    - 有序集合保存的所有元素长度小于默认值64字节
- skiplist特点：
    - 有很多层组成，由上到下节点数逐渐密集，最上层的节点最稀疏，跨度也最大。
    - 每一层都是一个有序链表，只扫包含两个节点，头节点和尾节点。
    - 每一层的每一个每一个节点都含有指向同一层下一个节点和下一层同一个位置节点的指针。
    - 如果一个节点在某一层出现，那么该以下的所有链表同一个位置都会出现该节点。
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/skiplist.png "照片")
- dict和skiplist各自的作用：
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/zsetStruct.png "照片")
- dict+skiplist最终存储结构如下：
![redis数据结构](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/redis/skiplistandhash.png "照片")
可以看到一个是dict结构，主要key是其集合元素，而value就是对应分值，而zkiplist作为跳跃表，按照分值排序，方便定位成员
## Hash类型
- hash对象只有同时满足以下条件，才会采用ziplist编码：
    - hash对象保存的键和值字符串长度都小于64字节
    - hash对象保存的键值对数量小于512