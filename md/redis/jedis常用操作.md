```
public class JedisDemo1 {

    public static void main(String[] args) {
        //创建jedis对象
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //测试
        String re = jedis.ping();
        System.out.println(re);
        jedis.flushDB();
        demo1(jedis);
    }

    private static void demo1(Jedis jedis) {
        jedis.set("name", "lucy");
        jedis.set("name", "ffff");
        String name = jedis.get("name");
        System.out.println(name);
        String mset = jedis.mset("k1", "v1", "k2", "v2");
        System.out.println(mset);
        List<String> mget = jedis.mget("k1", "k2");
        for (String key : mget) {
            System.out.println(key);
        }
        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
        //set
        jedis.sadd("set", "ffff", "ddd");
        Set<String> set = jedis.smembers("set");
        set.forEach(System.out::println);
        //hash
        jedis.hset("hash", "age", "20");
        Map<String, String> map = new HashMap<>();
        map.put("name", "jack");
        jedis.hset("hash", map);
        String val = jedis.hget("hash", "age");
        System.out.println(val);

        //zset
        jedis.zadd("zset",12,"shanghai");
        jedis.zadd("zset",11,"wuhan");
        System.out.println(jedis.zrange("zset",0,-1));
    }
}
```