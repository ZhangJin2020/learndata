# Mysql常用工具

## mysql官方工具

### mysql（客户端连接工具） mysql -u root -p 123456 -h 127.0.0.1 -P3306

### mysqladmin(mysql管理工具)功能与mysql客户端类似

### mysqlbinlog(日志管理工具)二进制日志转换成文本格式

首先，开启binlog:

在/etc/my.cnf中的[mysqld]下面直接增加内容

```ini
server_id=1918

log_bin = mysql-bin

binlog_format = ROW
```

退出并保存

重启mysql，则可在data目录下看到binlog

```shell
mysqlbinlog mysql-bin.000001 -v
```

### mysqlcheck(表维护工具)

用来检查，修复，优化和分析表，维护期间会加锁，不能进行ddl和dml操作