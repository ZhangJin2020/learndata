# 事务和锁

mysql支持对MyISAM和MEMORY存储引擎的表进行表级锁定，对BDB存储引擎的表进行页级锁定，对InnoDB存储引擎的表进行行级锁定。

commit后可以执行chain或者release操作，前者表示提交后重新开启 一个隔离级别相同的事务，后者表示提交后断开和客户端的连接。

关闭auto commit后执行的操作都需要显式commit才会提交，包括没有手动开启事务时执行的操作。

lock方式加的表锁，不支持rollback回滚操作。lock tables test read/WRITE;

