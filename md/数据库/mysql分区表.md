# mysql分区表

## 查看是否启用分区支持

```sql
select * from information_schema.plugins where PLUGIN_NAME='partition'\G;
*************************** 1. row ***************************
           PLUGIN_NAME: partition
        PLUGIN_VERSION: 1.0
         PLUGIN_STATUS: ACTIVE
           PLUGIN_TYPE: STORAGE ENGINE
   PLUGIN_TYPE_VERSION: 50720.0
        PLUGIN_LIBRARY: NULL
PLUGIN_LIBRARY_VERSION: NULL
         PLUGIN_AUTHOR: Mikael Ronstrom, MySQL AB
    PLUGIN_DESCRIPTION: Partition Storage Engine Helper
        PLUGIN_LICENSE: GPL
           LOAD_OPTION: ON
1 row in set (0.00 sec)
```

上图中plugin_status为active即已开启分区支持。mysql支持大部分分区设置分区表，同一个mysql服务器甚至同一个数据库中可以对不同分区表使用不同存储引擎，但是同一个分区表只能使用一种存储引擎。

## 作用

以下场景中能起到作用：

（1）表非常大以至于无法全部都放到内存中，或者只在表的最后部分放了热点数据，其他都是历史数据

（2）分区表数据更容易维护。想删除大量数据可以用清除整个分区的方式，还可以对单独分区进行优化、检查和修复等

（3）分区表数据可以分布在不同物理设备上，高效地利用硬件设备

（4）可以用分区表避免某些特殊的瓶颈，如innodb的单个索引的互斥访问、ext3文件系统的inode锁竞争等

（5）如果需要可以恢复和备份单独分区

mysql5.7中可以显式的指定查询某一分区的数据。

## 原理

分区表由多个相关的底层表来实现，存储引擎管理分区的底层表和管理普通表一样，我们也可以直接访问各个分区，分区表索引是在各个底层表上加上完全相同的索引。从存储引擎的角度底层表和普通表没有任何不同，存储引擎也无须知道是普通表还是分区表的一部分。

select查询分区表时，分区层先打开并锁住所有底层表，优化器判断是否可以过滤掉部分分区，然后调用存储引擎接口访问各分区数据。

insert操作和delete操作，先打开并锁住所有底层表，确定要插入的/更新数据对应的分区，执行操作

update操作，先打开并锁住底层表，确定分区，取出数据并更新，再判断更新后的数据应该放在哪个分区，最后对底层表进行写入操作，并删除原数据。

## 分区表的类型

主要有以下六种：

（1）RANGE分区：基于给定的连续区间范围分区

（2）LIST分区：类似RANGE分区，基于枚举出的值列表分区，非连续

（3）COLUMNS分区：类似于RANGE和LIST,区别在于分区键可以是多列还可以是非整数。

（4）HASH分区：基于给定的分区个数，把数据取模分布到各个分区

（5）KEY分区：类似于HASH分区，但使用MYSQL提供的哈希函数。

（6）子分区：在主分区下再做一层分区。

mysql5.7中要求RANGE,LIST,HASH分区的分区键必须是INT类型，但KEY和COLUMNS分区例外，可以使用其他类型（BLOB和TEXT除外）作为分区键。

分区键不能使用主键/唯一键之外的其他字段，除非没有主键/唯一键。

## RANGE分区
```
CREATE TABLE sales (

​	order_date DATETIME NOT NULL,

​	....

)	ENGINE=InnoDB  PARTITION  BY  RANGE(**YEAR**(order_date)) (

​	PARTITION  p_2010  VALUES LESS THAN  (2010),

​	PARTITION  p_2011  VALUES  LESS  THAN  (2011),

​	PARTITION  p_catchall  VALUES LESS  THAN MAXVALUE  );
```
将每一年的销售额放在不同分区里。PARTITION 表达式可以使用各种函数，但是返回的值要是一个确定的整数，且不能是一个常数。

mysql还支持键值、哈希和列表分区，有些还支持子分区。

根据时间分区，最近一年的数据访问非常频繁，会导致大量互斥锁竞争，使用哈希子分区可以将数据切成多个小片，降低互斥量的竞争问题。

```
mysql> create table emp(id int not null) partition by range(id)
	(partition p0 values less than(10),
		partition p1 values less than (20),
		partition p2 values less than MAXVALUE);
Query OK, 0 rows affected (1.41 sec)

mysql> insert into emp values(1);
Query OK, 1 row affected (0.16 sec)

mysql> insert into emp values(11);
Query OK, 1 row affected (0.04 sec)

mysql> insert into emp values(21);
Query OK, 1 row affected (0.12 sec)

explain partitions select count(*) from emp where id >20\G;
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: emp
   partitions: p2
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 1
     filtered: 100.00
        Extra: Using where
1 row in set, 2 warnings (0.00 sec)
```

删除分区：

```
mysql> select * from emp;
+----+
| id |
+----+
|  1 |
| 11 |
| 21 |
+----+
3 rows in set (0.00 sec)

mysql> alter table emp drop partition p0;
Query OK, 0 rows affected (1.05 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from emp;
+----+
| id |
+----+
| 11 |
| 21 |
+----+
2 rows in set (0.00 sec)

mysql> select partition_name,partition_method from information_schema.partitions where table_name='emp';
+----------------+------------------+
| partition_name | partition_method |
+----------------+------------------+
| p1             | RANGE            |
| p2             | RANGE            |
+----------------+------------------+
2 rows in set (0.00 sec)

mysql> select * from emp partition(p2);
+----+
| id |
+----+
| 21 |
+----+
1 row in set (0.00 sec)
```

5.5 版本开始，改进了RANGE分区功能，提供了RANGE COLUMNS分区支持非INT类型作为分区键，如DATE类型(PARTITION BY RANGE COLUMNS (create_date)( PARTITION p0 VALUES LESS THAN ('1990-01-01') ...))

show table status可以看出表是不是分区表

## LIST分区

```
create table expense(id int) 
	partition by list(id)(
		partition p0 values in(0,1),
		partition p1 values in(2,3));
		
mysql> insert into expense values(1);
Query OK, 1 row affected (0.05 sec)

mysql> insert into expense values(5);
ERROR 1526 (HY000): Table has no partition for value 5
```

LIST分区不存在RANGE分区中LESS THAN那样的定义方式，必须全部列举。插入不存在的会报错。

## COLUMNS分区

5.5版本后引入的，5.5版本之前RANGE和LIST分区都只支持整数。

COLUMNS分区可以细分为RANGE COLUMNS和LIST COLUMNS分区，两者都支持整数，日期，字符串三大数据类型。

5.7版本中，只支持一个或多个字段名作为分区键，不支持表达式作为分区键。

多个字段是基于元组的比较，其实就是多列排序后根据结果存放数据。

## HASH分区

用来分散热点读，确保数据在各分区中尽量均匀分布，对一个表执行HASH分区时，MYSQL会对分区键应用一个散列函数，用来确定数据应该放在哪个分区。

支持两种HASH分区，即常规HASH分区和线性HASH分区。常规方法采用取模运算，线性分区使用线性的2的幂的运算方法

常规HASH分区表的创建语法为 HASH (expr) PARTITIONS num,其中expr是某列值或者某个表达式的值，为整数，num为一个非负整数。创建一个四分区的表：

```sql
mysql> create table hashemp(id int) partition by hash(id) partitions 4;
Query OK, 0 rows affected (1.14 sec)

mysql> insert into hashemp values(1);
Query OK, 1 row affected (0.12 sec)

mysql> insert into hashemp values(2);
Query OK, 1 row affected (0.12 sec)

mysql> insert into hashemp values(3);
Query OK, 1 row affected (0.09 sec)

mysql> insert into hashemp values(4);
Query OK, 1 row affected (0.03 sec)
```

分区编号计算方法为N=MOD(expr, num)，即为表中的id对4取模的结果

```
mysql> explain partitions select * from hashemp where id=1;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p1         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from hashemp where id=2;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p2         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from hashemp where id=3;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p3         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from hashemp where id=4;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p0         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from hashemp where id=5;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p1         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from hashemp where id=6;
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | hashemp | p2         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+---------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

```

表达式expr可以是mysql中任何函数或表达式，返回整数即可，但每次对表进行增删改操作时，表达式都要计算一次，因此mysql也不推荐使用涉及多列的哈希表达式，会引起性能问题。

常规hash分区在改变分区数量时，需要对表中绝大部分原有数据进行重新分区，如分区增加到五个，分区数为MOD(expr, 5)，重新计算分区代价太大，不适合需要灵活变动分区数量的情形，线性HASH分区的出现解决了这个问题。

语法：PARTITION BY LINEAR HASH(id) PARTITIONS 4;

创建一个6个分区的表：

```
mysql> create table lhashemp(id int) partition by linear hash(id) partitions 6;
Query OK, 0 rows affected (1.67 sec)
```

分区号N计算方法如下：

（1）找到HASH(expr) PARTITIONS num中 大于等于num最小的2的幂，记为V，本例中大于等于6的2的幂为2的三次方，V=8

（2）将expr的值同(V-1)进行位与操作，本例中即为id列值与V-1=7进行位于操作，其实就是保留二进制的低三位，记为N

（3）若N<num，N即为分区号，若N>=num，计算方法为，将expr值同(Ceiling(V/2)-1)进行位与操作，本例中即为3，即只保留低二位

分区号计算方法就清楚了，id对应的二进制低三位小于6时(000——101)，低三位即为分区数，大于或者等于6时(110,111)，只保留低二位（10，11），则当id从1开始递增时，分区数依次为(1,2,3,4,5,2,3),0,1,2...再往后即为括号中内容的循环。

```
mysql> explain partitions select * from lhashemp where id=1;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p1         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.01 sec)

mysql> explain partitions select * from lhashemp where id=2;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p2         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=3;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p3         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=4;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p4         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=5;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p5         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=6;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p2         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=7;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p3         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=8;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p0         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)

mysql> explain partitions select * from lhashemp where id=9;
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
| id | select_type | table    | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra       |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
|  1 | SIMPLE      | lhashemp | p1         | ALL  | NULL          | NULL | NULL    | NULL |    1 |   100.00 | Using where |
+----+-------------+----------+------------+------+---------------+------+---------+------+------+----------+-------------+
1 row in set, 2 warnings (0.00 sec)
```

HASH分区个数是2的幂时，线性HASH分区和普通HASH分区结果一致

线性HASH分区优点是分区维护（增减，合并，拆分）时，mysql能处理的更迅速，缺点是没有常规分区数据分布那么均衡。

当分区数改变时，如由6增加到7，id由1递增对应的分区数一次为(1,2,3,4,5,6,3),0,1,2...循环，只有id低三位为110的记录由分区p2迁移到新分区，其他不变。当分区增加导致V变大时，如8分区变成9分区，8分区为(1,2,3,4,5,6,7,0)循环，9分区为(1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,0)循环，也只是将低四位为1000的记录由分区p0迁移到p8

分区表文件存储：

<img src="C:\Users\jin\AppData\Roaming\Typora\typora-user-images\image-20211203102127314.png" alt="image-20211203102127314" style="zoom:80%;" />

分区表锁竞争

事务1：

```
mysql> select * from hashemp;
+------+
| id   |
+------+
|    4 |
|    1 |
|    2 |
|    3 |
+------+
4 rows in set (0.00 sec)

mysql> begin;
Query OK, 0 rows affected (0.00 sec)

mysql> update hashemp set id=1 where id=1;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 1  Changed: 0  Warnings: 0

mysql>
```

锁定id=1所在分区p1，

事务2更新其他分区不受影响，更新p1分区会等待锁释放：

```
mysql> begin;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from hashemp;
+------+
| id   |
+------+
|    4 |
|    1 |
|    2 |
|    3 |
+------+
4 rows in set (0.00 sec)

mysql> update hashemp set id =2 where id=2;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 1  Changed: 0  Warnings: 0

mysql> update hashemp set id =2 where id=1;
ERROR 1205 (HY000): Lock wait timeout exceeded; try restarting transaction
```

