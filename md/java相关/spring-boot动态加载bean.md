使用ConfigurableApplicationContext类的removeBeanDefinition和registerBeanDefinition方法。
```
package com.dyna.service.impl;

import com.dyna.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    private ApplicationContext applicationContext;

    private static Class beanClass;

    TestServiceImpl() throws ClassNotFoundException {
        beanClass = Class.forName("com.dyna.pojo.MyBean");
    }

    public Object getBean(String name) {
        return applicationContext.getBean(name, beanClass);
    }

    public void addBean(String beanName, String property) {
        registerBean(beanName, beanClass, property);
    }

    private void registerBean(String name, Class clazz, Object... args) {
        ConfigurableApplicationContext context = (ConfigurableApplicationContext) applicationContext;
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(clazz);
        for (Object arg : args) {
            beanDefinitionBuilder.addConstructorArgValue(arg);
        }
        BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();
        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) context.getBeanFactory();
        if (context.containsBean(name)) {
            log.info("bean:[{}]在系统中已存在,接下来对其进行替换操作", name);
            beanFactory.removeBeanDefinition(name);
        }
        beanFactory.registerBeanDefinition(name, beanDefinition);
    }
}
```