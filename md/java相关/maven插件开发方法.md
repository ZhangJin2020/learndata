# 开发maven插件的步骤
## 引入maven依赖
```
<dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
      <version>2.0</version>
    </dependency>
    <dependency>
      <groupId>org.apache.maven.plugin-tools</groupId>
      <artifactId>maven-plugin-annotations</artifactId>
      <version>3.2</version>
      <scope>provided</scope>
    </dependency>
```
## 创建实现类，继承org.apache.maven.plugin.AbstractMojo.class
如：
```
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Mojo(name = "compress")
public class CompressCmd extends AbstractMojo {

    @Parameter
    private String inputPath;

    @Parameter
    private String outputPath;
    @Parameter(defaultValue = "jpg")
    private String format;
    @Parameter(defaultValue = "0.5")
    private Double scale;


    public void execute() throws MojoExecutionException, MojoFailureException {

        File file = new File(inputPath);
        if(!(file.exists()&&file.isDirectory())){
            throw new RuntimeException("'inputPath' should be valid.");
        }
        File[] files = file.listFiles();
        File des = new File(outputPath);
        if(!des.exists()){
            boolean mkdirs = des.mkdirs();
            if(mkdirs){
                getLog().info("'outputPath not exist, created.");
            }
        }
        if(!des.isDirectory()){
            throw new RuntimeException("'outputPath' should be valid.");
        }
        try {
            Thumbnails.of(files)
                    .scale(scale)
                    .outputFormat(format)
                    .toFiles(des, Rename.NO_CHANGE);
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
        getLog().info("success!");
    }
}

```
上述代码实现了一个压缩图片的maven插件，允许用户定义四个参数，引用该插件的方式如下，在maven项目中pom.xml添加：
```
<build>
    <plugins>
        <plugin>
            <groupId>org.jin</groupId>
            <artifactId>pic-compress</artifactId>
            <version>1.0-SNAPSHOT</version>
            <configuration>
                <inputPath>./src/main/resources/pic</inputPath>
                <outputPath>./src/main/resources/compressed</outputPath>
                <format>jpg</format>
                <scale>0.1</scale>
            </configuration>
        </plugin>
    </plugins>
</build>
```
然后点击maven插件中的```pic-compress-maven-plugin:compress```来执行
![maven.png](https://gitlab.com/ZhangJin2020/learndata/-/raw/master/pic/maven.png)