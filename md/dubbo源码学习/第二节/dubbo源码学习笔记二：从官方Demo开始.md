本节将从代码库中的`dubbo-demo`板块入手，了解官方demo逻辑。
# 1、dubbo-demo模块概述
dubbo-demo模块包括三个非常基础的Dubbo实例项目，分别是：使用XML配置的Demo实例，使用注解配置的Demo实例，以及使用API配置的Demo实例。本节将以这三个项目为入口来说明Dubbo的基本用法。

![](https://files.mdnice.com/user/57877/515b3fa9-2f4d-48e7-8e6e-341cf80375db.png)


由于用例需要启动provider、consumer服务，需要注册中心的配合，因此在运行demo之前，我们需要在本地启动zookeeper。

如果你已经有zookeeper环境，可以跳过这一部分

# 2、zookeeper 下载和启动

首先，下载zookeeper的资源包，地址： `https://archive.apache.org/dist/zookeeper/zookeeper-3.4.14/`,下载完成后解压。

解压完成后，进入`zookeeper-3.4.14`目录，我们需要对默认的配置文件进行修改。

将`zookeeper-3.4.14/conf`目录下的zoo_sample.cfg重命名为zoo.cfg，之后打开这个文件，将dataDir后的目录改为有效目录，如`D:\zookeeper-3.4.14\data`

改完就可以进入`zookeeper-3.4.14/bin`目录，执行相应的文件来启动zookeeper了，如果是windows环境，双击zkServer.cmd即可完成启动，如果是macod或者linux系统，相应的需要执行zkServer.sh文件来启动

日志中出现 
```
2024-11-11 14:18:50,549 [myid:] - INFO  [main:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:2181
```
即表示启动启动成功，端口为2181.

如果想要进一步验证服务是否启动成功，可以双击`zookeeper-3.4.14/bin`目录下的zkCli.cmd（linux和macos系统使用zkCli.sh)来运行命令行客户端工具，窗口中出现
```
[zk: localhost:2181(CONNECTED) 0]
```
即表示连接成功，可以执行一些zkCli的命令进行相应的操作了。

# 3、Demo分析
在使用Dubbo之前，需要一个业务接口，即Provider和Consumer之间的一个约定，双方需要知道，Provider要提供什么能力，Consumer需要调用什么能力，参数是什么，返回是什么。在demo模块中，这个业务接口被放在了dubbo-demo-interface子模块中。
![](https://files.mdnice.com/user/57877/7c52e093-2107-4ee9-827f-76aeffad4190.png)
可以看到该子模块中有两个接口，DemoService和GreetingService，其中GreetingService中只有一个sayHello方法，这个接口在其他模块中没有实现过，因此我们直接看DemoService这个接口，具体接口逻辑定义如下：

先来看GreetingService这个接口，比较简单，
```
public interface DemoService {

    /**
     * 定义了一个sayHello接口方法，传入name参数，返回string类型
     * @return
     */
    String sayHello(String name);

    /**
     * 定义一个sayHelloAsync方法，用CompletableFuture实现对异步调用的支持，同时此处的接口方法用defalut修饰，因此可以有方法体(java 1.8特性)
     * @return
     */
    default CompletableFuture<String> sayHelloAsync(String name) {
        return CompletableFuture.completedFuture(sayHello(name));
    }
}
```

## 3.1 Demo 1：基于XML配置
在dubbo-demo-xml模块中，提供了基于Spring XML的Provider和Consumer。其代码结构如下图：
![](https://files.mdnice.com/user/57877/c70c849b-3f31-41dd-b566-e55938a4dafd.png)
该模块又分为consumer和provider两个子模块，两个子模块中都引用了dubbo-demo-interface模块，可以预想到，Provider是实现了DemoService接口，提供了具体的逻辑作为服务提供方，而Consumer是通过DemoService接口来进行服务方法的调用。
### 3.1.1Provider端
来看具体的配置和代码逻辑，先看Provider端，在resouce目录下的`/spring/dubbo-provider.xml`文件中，有如下定义：
```
<!--metadata-type为remote指定元数据保存到元数据中心，-->
<dubbo:application metadata-type="remote" name="demo-provider"/>

<!--元数据中心地址-->
<dubbo:metadata-report address="zookeeper://127.0.0.1:2181"/>

<!--注册中心的地址-->
<dubbo:registry address="zookeeper://127.0.0.1:2181"/>

<!--定义dubbo的协议-->
<dubbo:protocol name="dubbo"/>

<!--注入spring bean-->
<bean id="demoService" class="org.apache.dubbo.demo.provider.DemoServiceImpl"/>

<!--定义dubbo服务对应的接口和实现类-->
<dubbo:service interface="org.apache.dubbo.demo.DemoService" ref="demoService"/>
```
元数据中心是dubbo2.7版本之后新增的功能，主要是为了减轻注册中心的压力，将部分存储在注册中心的内容放到元数据中心。元数据中心的数据只是给自己使用的，改动不需要告知对端，比如服务端修改了元数据，不需要通知消费端。这样注册中心存储的数据减少，同时大大降低了因为配置修改导致注册中心频繁通知监听者，从而大大减轻注册中心的压力。
Application.java:
```
public class Application {
    public static void main(String[] args) throws Exception {
        // 指定spring配置文件路径，加载对应的类
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring/dubbo-provider.xml");
        // 启动spring容器
        context.start();
        // 读取输入，此处作用是让程序不马上退出
        System.in.read();
    }
}
```
DemoService.java:
```
/**
 * 实现DemoService接口，实现服务逻辑
 */
public class DemoServiceImpl implements DemoService {
    private static final Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);

    /**
     * 同步处理的接口
     */
    @Override
    public String sayHello(String name) {
        // 打印日志，参数name和消费方的ip地址
        logger.info("Hello " + name + ", request from consumer: " + RpcContext.getContext().getRemoteAddress());
        // sleep 1秒
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 返回结果，带上provider的ip地址
        return "Hello " + name + ", response from provider: " + RpcContext.getContext().getLocalAddress();
    }

    /**
     * 异步处理的接口
     */
    @Override
    public CompletableFuture<String> sayHelloAsync(String name) {
        // 用CompletableFuture实现异步调用，返回CompletableFuture包装的对象
        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            // 返回固定字符串
            return "async result";
        });
        return cf;
    }
}
```
`dubbo.properties`配置文件中还有一行配置
```
# 指定了dubbo的QOS服务端口
dubbo.application.qos.port=22222
```
QoS（Quality of Service）服务，用于提供服务的监控和管理功能。
### 3.1.2 Consumer端
`/spring/dubbo.consumer.xml`文件配置：
```
<dubbo:application name="demo-consumer"/>

<!--注册中心地址-->
<dubbo:registry address="zookeeper://127.0.0.1:2181"/>

<!--引用服务名称和接口-->
<dubbo:reference id="demoService" check="false" interface="org.apache.dubbo.demo.DemoService"/>
```
dubbo.properties配置文件中和provider类似地指定了元数据端口，不再赘述
Consumer端只有一个java类，Application.java:
```
public class Application {
    /**
     * In order to make sure multicast registry works, need to specify '-Djava.net.preferIPv4Stack=true' before
     * launch the application
     */
     /**
      * 以上为官方注释
      */
    public static void main(String[] args) throws Exception {
        // 指定配置文件路径并启动spring容器
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring/dubbo-consumer.xml");
        context.start();
        // 从容器中获取接口的实现类
        DemoService demoService = context.getBean("demoService", DemoService.class);
        // 调用异步方法，获取CompletableFuture包装的结果，并打印到控制台
        CompletableFuture<String> hello = demoService.sayHelloAsync("world");
        System.out.println("result: " + hello.get());
    }
}
```
### 3.1.3 启动验证
首先，运行Provider端的Application.java中的Main方法，启动服务提供方，将服务提供方注册到注册中心
启动成功后控制台输出如下：
![](https://files.mdnice.com/user/57877/74276005-5229-4b13-a2e9-5c0d86dc6524.png)
随后，保持Provider端启动状态，再启动Consumer端，同样地，执行Consumer端Application类下的main方法，执行成功后控制台输出如下：
![](https://files.mdnice.com/user/57877/dbb686d9-864d-4eba-a24b-b1ff016d09b2.png)
Consumer端main方法执行完成就退出了，可以从Consumer端控制台日志中找到方法调用Provider成功后打印的日志：
```
result: async result
result: Hello world2, response from provider: 10.177.132.32:20880
```
此时再看Provider端的日志，可以看到如下打印：
```
[11/11/24 18:07:20:696 CST] NettyServerWorker-5-2  INFO netty4.NettyServerHandler:  [DUBBO] The connection of /10.177.132.32:4165 -> /10.177.132.32:20880 is established., dubbo version: , current host: 10.177.132.32
[11/11/24 18:07:21:237 CST] DubboServerHandler-10.177.132.32:20880-thread-8  INFO provider.DemoServiceImpl: Hello world2, request from consumer: /10.177.132.32:4165
[11/11/24 18:07:22:242 CST] DubboServerHandler-10.177.132.32:20880-thread-9  INFO provider.DemoServiceImpl: Hello world2, request from consumer: /10.177.132.32:4165
[11/11/24 18:07:23:281 CST] NettyServerWorker-5-2  WARN transport.AbstractServer:  [DUBBO] All clients has disconnected from /10.177.132.32:20880. You can graceful shutdown now., dubbo version: , current host: 10.177.132.32
[11/11/24 18:07:23:282 CST] NettyServerWorker-5-2  INFO netty4.NettyServerHandler:  [DUBBO] The connection of /10.177.132.32:4165 -> /10.177.132.32:20880 is disconnected., dubbo version: , current host: 10.177.132.32
```
## 3.2 Demo2：基于注解配置
### 3.2.1 Provider端
dubbo-demo-annotation模块是基于Spring注解配置的实例，就是将XML配置文件中的那些配置通过注解的方式来实现
Application.java文件:
```
public class Application {
    public static void main(String[] args) throws Exception {
        // spring上下文容器
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProviderConfiguration.class);
        context.start();
        // 保持Provider端程序运行状态
        System.in.read();
    }

    @Configuration
    // 扫描实现类DemoServiceImpl所在的包
    @EnableDubbo(scanBasePackages = "org.apache.dubbo.demo.provider")
    // 指定properties配置文件路径
    @PropertySource("classpath:/spring/dubbo-provider.properties")
    static class ProviderConfiguration {
        @Bean
        public RegistryConfig registryConfig() {
            RegistryConfig registryConfig = new RegistryConfig();
            registryConfig.setAddress("zookeeper://127.0.0.1:2181");
            return registryConfig;
        }
    }
}

```
`/spring/dubbo-provider.properties`文件内容：
```
dubbo.application.name=dubbo-demo-annotation-provider
dubbo.protocol.name=dubbo
dubbo.protocol.port=20880
```
只是spring的xml配置文件使用注解替换了，dubbo的properites配置文件仍然保留

实现类DemoServiceImpl内容：
```
/**
 * 类的主体基本没有变化，需要注意这里是Service注解是org.apache.dubbo.config.annotation.Service，不是spring的Service注解
 */
@Service
public class DemoServiceImpl implements DemoService {
    private static final Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);

    @Override
    public String sayHello(String name) {
        logger.info("Hello " + name + ", request from consumer: " + RpcContext.getContext().getRemoteAddress());
        return "Hello " + name + ", response from provider: " + RpcContext.getContext().getLocalAddress();
    }

    @Override
    public CompletableFuture<String> sayHelloAsync(String name) {
        return null;
    }
}
```
这种方式去掉了spring的配置文件，用@EnableDubbo注解指定实现类的路径，实现类添加dubbo的Service类注解。
### 3.2.2 Consumer端
Application.java类：
```
public class Application {
    /**
     * In order to make sure multicast registry works, need to specify '-Djava.net.preferIPv4Stack=true' before
     * launch the application
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        DemoService service = context.getBean("demoServiceComponent", DemoServiceComponent.class);
        String hello = service.sayHello("world");
        System.out.println("result :" + hello);
    }

    @Configuration
    // 扫描消费端的dubbo相关类
    @EnableDubbo(scanBasePackages = "org.apache.dubbo.demo.consumer.comp")
    @PropertySource("classpath:/spring/dubbo-consumer.properties")
    // spring的扫描注解，扫描此包下的类
    @ComponentScan(value = {"org.apache.dubbo.demo.consumer.comp"})
    static class ConsumerConfiguration {

    }
}
```
DemoServiceCompoment类
```
package org.apache.dubbo.demo.consumer.comp;

@Component("demoServiceComponent")
public class DemoServiceComponent implements DemoService {
    /**
     * Reference注解全限定名为org.apache.dubbo.config.annotation.Reference
     */
    @Reference
    private DemoService demoService;

    @Override
    public String sayHello(String name) {
        return demoService.sayHello(name);
    }

    @Override
    public CompletableFuture<String> sayHelloAsync(String name) {
        return null;
    }
}

```
### 3.2.3 启动验证
和上一节一样，先启动Provider端，随后启动Consumer端，可以观察到如下日志
Consumer端：
```
result :Hello world, response from provider: 10.177.132.32:20880
```
Provider端有如下日志：
```
[12/11/24 14:28:58:289 CST] NettyServerWorker-3-1  INFO netty4.NettyServerHandler:  [DUBBO] The connection of /10.177.132.32:4186 -> /10.177.132.32:20880 is established., dubbo version: , current host: 10.177.132.32
[12/11/24 14:28:58:857 CST] DubboServerHandler-10.177.132.32:20880-thread-2  INFO provider.DemoServiceImpl: Hello world, request from consumer: /10.177.132.32:4186
[12/11/24 14:28:58:954 CST] NettyServerWorker-3-1  WARN transport.AbstractServer:  [DUBBO] All clients has disconnected from /10.177.132.32:20880. You can graceful shutdown now., dubbo version: , current host: 10.177.132.32
[12/11/24 14:28:58:954 CST] NettyServerWorker-3-1  INFO netty4.NettyServerHandler:  [DUBBO] The connection of /10.177.132.32:4186 -> /10.177.132.32:20880 is disconnected., dubbo version: , current host: 10.177.132.32
```
## 3.3 Demo3：基于api的配置
适合于一些不能依赖spring框架的场景
### 3.3.1 Provider端
Application.class类
```
public class Application {
    public static void main(String[] args) throws Exception {
        if (isClassic(args)) {
            startWithExport();
        } else {
            startWithBootstrap();
        }
    }

    /**
     * 通过main方法入参判断是否是classic模式
     */
    private static boolean isClassic(String[] args) {
        return args.length > 0 && "classic".equalsIgnoreCase(args[0]);
    }
    
    /**
     * 使用DubboBootstrap启动服务端
     */
    private static void startWithBootstrap() {
        // 设置服务信息
        ServiceConfig<DemoServiceImpl> service = new ServiceConfig<>();
        service.setInterface(DemoService.class);
        service.setRef(new DemoServiceImpl());
        // 创建DubboBootstrap对象
        DubboBootstrap bootstrap = DubboBootstrap.getInstance();
        // 设置注册中心地址，绑定服务
        bootstrap.application(new ApplicationConfig("dubbo-demo-api-provider"))
                .registry(new RegistryConfig("zookeeper://127.0.0.1:2181"))
                .service(service)
                .start()
                .await();
    }

    private static void startWithExport() throws InterruptedException {
        // 创建ServiceConfig类型对象，然后绑定需要的参数
        ServiceConfig<DemoServiceImpl> service = new ServiceConfig<>();
        service.setInterface(DemoService.class);
        service.setRef(new DemoServiceImpl());
        service.setApplication(new ApplicationConfig("dubbo-demo-api-provider"));
        service.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        // 发布服务
        service.export();

        System.out.println("dubbo service started");
        new CountDownLatch(1).await();
    }
}
```
### 3.3.2 Consumer端
```

public class Application {
    public static void main(String[] args) {
        if (isClassic(args)) {
            runWithRefer();
        } else {
            runWithBootstrap();
        }
    }

    private static boolean isClassic(String[] args) {
        return args.length > 0 && "classic".equalsIgnoreCase(args[0]);
    }

    private static void runWithBootstrap() {
        ReferenceConfig<DemoService> reference = new ReferenceConfig<>();
        reference.setInterface(DemoService.class);
        reference.setGeneric("true");

        DubboBootstrap bootstrap = DubboBootstrap.getInstance();
        bootstrap.application(new ApplicationConfig("dubbo-demo-api-consumer"))
                .registry(new RegistryConfig("zookeeper://127.0.0.1:2181"))
                .reference(reference)
                .start();

        DemoService demoService = ReferenceConfigCache.getCache().get(reference);
        String message = demoService.sayHello("dubbo");
        System.out.println(message);

        // generic invoke
        GenericService genericService = (GenericService) demoService;
        Object genericInvokeResult = genericService.$invoke("sayHello", new String[] { String.class.getName() },
                new Object[] { "dubbo generic invoke" });
        System.out.println(genericInvokeResult);
    }

    private static void runWithRefer() {
        ReferenceConfig<DemoService> reference = new ReferenceConfig<>();
        reference.setApplication(new ApplicationConfig("dubbo-demo-api-consumer"));
        reference.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        reference.setInterface(DemoService.class);
        DemoService service = reference.get();
        String message = service.sayHello("dubbo");
        System.out.println(message);
    }
}

```
### 3.3.3 启动验证
略