# 写在前面
源码阅读是非常枯燥乏味的，这也是为什么对源码的了解通常会成为衡量开发人员水平很重要的依据，有个成语说的好，曲高和寡。

如果是自己将代码克隆下来闷头研究，除非本身技术足够扎实，且有较多的时间和耐心，否则很难坚持下去

遇到问题查阅相关资料时，如果只是每次针对问题用搜索引擎找答案，常常只能获取到一些片面的结果

本系列文章将从环境搭建开始，一步步揭开dubbo的面纱，让源码不再神秘。同时，会尽量通过丰富的Demo演示，让你能对问题了解更透彻。

好了，废话不多说，从此刻起，一起开始dubbo的源码之旅吧。

# dubbo简介
Apache Dubbo是一款高性能、轻量级的开源java RPC框架

有三大核心能力：
- 面向接口的远程方法调用
- 可靠、只能的容错和负载均衡
- 服务自动注册和发现能力

Dubbo是由阿里开源，后来加入了Apache基金会，很多大厂都在采用，且在其上进行了二次开发。
# Dubbo架构简介
为了便于理解和学习，一起回顾一下Dubbo的核心架构和角色：

![](https://files.mdnice.com/user/57877/383927b6-ecb3-4bd2-8689-f993b26f6cdb.png)
- Registry：注册中心。负责服务地址的登记与查找，服务Provider和Consumer只在启动时和注册中心交互。注册中心通过长连接感知Provider的存在，在Provider出现宕机的时候，注册中心会推送事件通知给Consumer
- Provider: 服务提供者（通常是zookeeper等）。在启动时，会向注册中心进行注册操作，将自己服务的地址和相关配置信息添加到注册中心中，告诉注册中心自己可以提供服务了。
- Consumer：服务消费者。在启动时，会向注册中心进行订阅操作，订阅操作会从注册中心中获取Provider注册的地址，并在注册中心添加相应的监听器（后面会提到，用于监听Provider地址的变化）。获取到地址后，如果地址是多个，Consumer会通过负载均衡算法选择其中一个地址并与其建立连接，发起RPC调用。如果Provider的地址发生变更，Consumer会通过之前添加的监听器获取到最新的Provider地址信息，进行相应的调整。Consumer与Provider建立的是长连接，且Consumer会缓存Provider信息，所以一旦连接建立，即使注册中心宕机，也不会影响已运行的Provider和Consumer。
- Monitor：监控中心。用于统计服务的调用次数和调用时间。Provider和Consumer在运行过程中，会在内存中统计调用次数和调用时间，定时每分钟发送一次统计数据到监控中心，监控中心在架构图中不是必要角色，其宕机只会丢失监控数据，不会影响Provider，Consumer以及Registry的运行。


# 代码获取
## 开发工具：
- Intellij idea

dubbo作为一个Apache基金会的顶级项目，目前在github已有
的star，我们可以从github(或者如果你的网络状况不佳，可以用国内的gitee)获取到dubbo的最新代码
在idea中选择`File->New->Project from Version Control`

![](https://files.mdnice.com/user/57877/9007edbb-f6a0-4a0c-90cf-c28facb05ebc.png)
在弹出的窗口中粘贴项目对应的URL，点击clone按钮将项目克隆到本地，笔者使用的是gitee地址：
`https://gitee.com/apache/dubbo.git`，过程中可能需要输入gitee的账号密码

![](https://files.mdnice.com/user/57877/8007e95e-61f9-4954-a229-0418b966ad30.png)
克隆完成打开项目后可以从git窗口看到代码有相当多的分支，对应了不同的版本，本文创作时最新版本已经到了`3.2`，基于文档和参考文档的版本考虑，本系列笔记统一采用`2.7.7`版本，可以在git窗口中切换分支到`2.7.7`。

新版本的变化和新功能在后面的文章里也会提及。都会有一定了解。
# 模块功能简单介绍
本节将通过源码各模块和对应的包给大家简单介绍每个模块的大致作用，至于详细分析将在后续章节中展开：

2.7.7版本dubbo源码各模块结构如图所示：

![](https://files.mdnice.com/user/57877/b01d1106-d197-4783-98ab-04449a7c669d.png)
- dubbo-common模块。Dubbo的一个公共模块，其中有很多工具类以及公共逻辑，例如Dubbo SPI实现、时间轮实现、动态编译实现等。
- dubbo-remoting模块。Dubbo的远程通信模块，其中的子模块依赖各种开源组件实现远程通信。在dubbo-remoting-api模块中定义了该模块的核心抽象，其他模块是对api模块的具体实现
- dubbo-rpc模块。Dubbo中对远程调用协议进行抽象的模块，其中抽象了各种协议，dubbo-rpc-api子模块是核心抽象，其他模块对api模块进行了具体实现
- dubbo-cluster模块。Dubbo中负责管理集群的模块，提供了负载均衡、容错、路由等一系列集群相关的功能，最终目的是将多个Provider伪装成一个Provider，这样Consumer就可以像调用一个Provider一样调用Provider集群了
- dubbo-registry模块。Dubbo中负责和注册中心交互的模块，提供服务注册等能力。dubbo-registry-api子模块是顶层抽象，其他子模块是对api模块的具体实现。例如dubbo-registry-zookeeper模块
- dubbo- monitor模块，Dubbo的监控模块，主要用于统计服务调用次数、调用时间以及实现调用链跟踪服务。
- dubbo-config模块。Consumer和Provider服务中Dubbo的相关配置都是由该模块进行解析的，用户只需了解Dubbo配置规则，不需了解实现细节
- dubbo-metadata模块。Dubbo的元数据模块，也是有一个子模块dubbo-metadata-api进行顶层抽象，其他子模块具体实现
- dubbo-configcenter模块。Dubbo的动态配置模块，主要负责外部配置以及服务治理规则的存储与通知。提供多个子模块用来接入多种开源服务发现组件。

