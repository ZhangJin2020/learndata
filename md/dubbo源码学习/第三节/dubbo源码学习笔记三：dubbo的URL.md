# 1 URL定义
在互联网领域，每个资源都有唯一的地址，该地址就叫URL(Uniform Resource Locator, 统一资源定位符)，它是互联网统一资源定位标志，也就是网络地址。

URL本质是一个特殊格式字符串。标准格式URL包含可以如下部分

`protocol://username:password@host:port/path?key=value&key=value`

- protocol: 协议。如常见的HTTP和HTTPS，在dubbo中，使用较多的是dubbo协议
- username/password:用户名/密码。在HTTP协议中多会在URL的协议之后直接携带用户名和密码的方式
- host/port: 主机名/端口号。
- path：请求路径。
- parameters：即`?`和后面的部分，表示参数键值对。
URL是dubbo中非常基础、非常核心的组件，源码中很多关键的方法都是用URL作为参数的（如Provider的注册、Consumer的订阅），通过解析URL参数得到想要的值，因此URL又被称为Dubbo的配置总线。

所以说，抓住了URL，就理解了半个dubbo。

# 2 Dubbo中的URL
## 2.1 Dubbo中的URL结构
Dubbo中使用URL来统一描述所有对象和配置信息，并贯穿在整个Dubbo框架之中。

接下来我们选取Provider注册过程中的一个URL来进行分析

在上一节的讨论的Demo代码中选择一个，启动后观察日志中URL的打印情况,可以看到如下打印信息：
```
[13/11/24 11:13:55:446 CST] main  INFO zookeeper.ZookeeperRegistry:  [DUBBO] Register: dubbo://10.177.132.45:20880/org.apache.dubbo.demo.DemoService?anyhost=true&application=dubbo-demo-api-provider&default=true&deprecated=false&dubbo=2.0.2&dynamic=true&generic=false&interface=org.apache.dubbo.demo.DemoService&methods=sayHello,sayHelloAsync&pid=11932&release=&side=provider&timestamp=1731467633962, dubbo version: , current host: 10.177.132.45
```
本节主要讨论URL的结构，我们选取`Register:`后打印的URL部分进行研究
```
dubbo://10.177.132.45:20880/org.apache.dubbo.demo.DemoService?anyhost=true&application=dubbo-demo-api-provider&default=true&deprecated=false&dubbo=2.0.2&dynamic=true&generic=false&interface=org.apache.dubbo.demo.DemoService&methods=sayHello,sayHelloAsync&pid=11932&release=&side=provider&timestamp=1731467633962
```
其各个部分的作用分别如下：

- `dubbo`: 协议部分，表示这个URL是dubbo协议
- `10.177.132.45:20880`: ip/端口号部分，明确了Provider的ip地址和端口号
- `org.apache.dubbo.demo.DemoService`,请求路径部分，这里是Provider端提供的服务接口的全限定名
- `?`及后面的部分，参数键值对，可以看到包含了很多内容

其中没有包括上文中提到的username/password部分，表示这个URL不需要用户名和密码
## 2.2 Dubbo中的URL类
Dubbo中URL类在dubbo-common模块下的`org.apache.dubbo.common`包下

其中一个构造函数：
```
public URL(String protocol,
               String username,
               String password,
               String host,
               int port,
               String path,
               Map<String, String> parameters,
               Map<String, Map<String, String>> methodParameters) {
        // 省略函数体
    }
```
可以看到构造函数参数和前文分析的URL构成基本一致。\
此外，`org.apache.dubbo.common`包中还有URL相关的工具类URLBuilder和URLStrParser，其中URLBuilder的作用是让使用者可以通过builder模式来构造URL对象，作用和lombok中的`@Builder`注解类似\
URLStrParser中提供了一些工具方法，如：`parseDecodedStr`
```
public static URL parseDecodedStr(String decodedURLStr) {
        Map<String, String> parameters = null;
        int pathEndIdx = decodedURLStr.indexOf('?');
        if (pathEndIdx >= 0) {
            parameters = parseDecodedParams(decodedURLStr, pathEndIdx + 1);
        } else {
            pathEndIdx = decodedURLStr.length();
        }

        String decodedBody = decodedURLStr.substring(0, pathEndIdx);
        return parseURLBody(decodedURLStr, decodedBody, parameters);
    }
```
可以看到，此方法的作用是将字符串类型的URL解析成dubbo的URL对象，具体解析方法是将URL字符串通过问号分割成后面的参数部分和前面的body部分，然后先通过`parseDecodedParams`方法将参数解析成Map类型，然后调用`parseURLBody`方法将前面部分解析完成。这两个方法的具体逻辑此处不作展开分析了。\
这个类中还有一些其他的工具类方法，如
```
public static URL parseEncodedStr(String encodedURLStr) {
    // 方法体省略
}
```
作用是将编码后的URL字符串解析成URL类型，如`?`编码后是`%3F`