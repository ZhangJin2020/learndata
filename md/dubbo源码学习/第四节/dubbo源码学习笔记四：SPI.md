# SPI机制
为了良好的扩展性，Dubbo采取了“微内核+插件”的插件化架构。这是一种面向功能拆分的可扩展架构，内核功能比较稳定，只负责管理插件的生命周期，不会因为系统功能扩展而不断修改。功能扩展全部封装到插件中。

微内核架构中，内核通常采用Factory，Ioc，OSGI等方式来管理插件的生命周期，在Dubbo中，采用SPI机制来加载插件，Dubbo SPI参考JDK原生SPI机制进行了性能优化以及功能增强。
# 1 JDK SPI
# 1.1 JDK SPI介绍
SPI(Service Provider Interface)主要是被框架开发人员使用的一种技术。例如使用JDBC访问数据库时我们会使用到java.sql.Driver接口，不同数据库对这个接口进行了各自的实现，在实际使用时，通过SPI来加载接口的实现。

JDK SPI加载机制如下：
当服务提供者提供了一种接口的实现类后，需要在Classpath下的META-INF/services/目录下创建一个以服务接口命名的配置文件，此配置文件记录了该jar包提供的服务接口的具体实现类名。当某个应用引入了该java包且需要使用该服务时，JDK SPI机制就会通过这个目录下的配置文件来获得具体的实现类名，进而进行类的加载和使用。

下面给出一个简单的demo来说明JDK SPI的原理
定义一个管理日志的接口Log，其中有一个打印日志的方法
，有两个实现类Log4j和Logback,实现了这个方法。

在项目的resources/META-INF/services/目录下创建配置文件，文件名为接口的全限定名`com.dubbo.test.spi.jdk.logdemo.inter.Log`，文件内容如下：
```
com.dubbo.test.spi.jdk.logdemo.impl.LogBack
com.dubbo.test.spi.jdk.logdemo.impl.Log4j
```
即文件内容为两个实现类的全限定名。

Log接口内容：
```
public interface Log {
    void log(String info);
}
```
两个实现类的内容：
```
public class Log4j implements Log {
    @Override
    public void log(String info) {
        System.out.println("Log4j: "+info);
    }
}
```
```
public class LogBack implements Log {
    @Override
    public void log(String info) {
        System.out.println("LogBack: "+info);
    }
}
```

随后创建启动类和main方法，在main方法中通过下列代码加载配置文件，创建全部Log接口的实现类，并调用他们的log方法
```
public class SpiTest {
    public static void main(String[] args) {
        ServiceLoader<Log> serviceLoader=ServiceLoader.load(Log.class);
        Iterator<Log> iterator = serviceLoader.iterator();
        while(iterator.hasNext()){
            iterator.next().log("JDK API");
        }
    }
}
```
上述代码输出如下：
```
LogBack: JDK API
Log4j: JDK API
```
## 1.2 JDK SPI源码分析
分析上节中的启动类SpiTest中的main方法，可以看出，JDK SPI核心在于`ServiceLoader.load(Class clazz)`方法，该方法接收一个接口类的class对象，返回这个接口的实现类的集合的迭代器iterator。本节我们将对该方法进行深入分析。
```
public static <S> ServiceLoader<S> load(Class<S> service) {
    // 获取当前线程的上下文类加载器
    ClassLoader cl = Thread.currentThread().getContextClassLoader();
    // 调用下面的两个参数的重载load方法
    return ServiceLoader.load(service, cl);
}

public static <S> ServiceLoader<S> load(Class<S> service,
                                            ClassLoader loader)
{
    // 调用ServiceLoader类的构造方法
    return new ServiceLoader<>(service, loader);
}
```
可以看到，`ServiceLoader.load`方法最终调用了`ServiceLoader`类的构造方法，该构造方法内容如下：
```
/**
 * 安全相关
 */
private final AccessControlContext acc;

private ServiceLoader(Class<S> svc, ClassLoader cl) {
    // 判断传入的Class对象是否是null，如果是null就报错
    service = Objects.requireNonNull(svc, "Service interface cannot be null");
    // 判断传入的ClassLoader是否是null，如果是null调用ClassLoader.getSystemClassLoader()来获取系统类加载器
    loader = (cl == null) ? ClassLoader.getSystemClassLoader() : cl;
    // 获取安全相关的
    acc = (System.getSecurityManager() != null) ? AccessController.getContext() : null;
    // 调用reload方法
    reload();
}
```
下面看reload方法的内容：
```
/**
 * 缓存加载完的实现类，value是实现类对象
 */
private LinkedHashMap<String,S> providers = new LinkedHashMap<>();
/**
 * 懒加载迭代器
 */
private LazyIterator lookupIterator;
/**
 * 当前被加载的接口class对象
 */
private final Class<S> service;

public void reload() {
    // 清空实现类对象缓存map
    providers.clear();
    // 新建迭代器类
    lookupIterator = new LazyIterator(service, loader);
}
```
可以看到，最终只是创建了一个LazyIterator的迭代器类，这一步并没有去实例化接口的实现类。从迭代器的类名顾名思义，SPI机制接口的实现类是懒加载的。

我们回到main方法接着向下看，获取到ServiceLoader类型对象后，调用了ServiceLoader.iterator()方法获取到迭代器类，后续调用迭代器的.next()方法来获取实例对象的。我们来看看迭代器的具体实现。

```
public Iterator<S> iterator() {
    // 返回一个匿名内部类对象
    return new Iterator<S>() {

        // 获取provider缓存的迭代器
        Iterator<Map.Entry<String,S>> knownProviders
                = providers.entrySet().iterator();

        public boolean hasNext() {
            // 如果缓存中有，返回true
            if (knownProviders.hasNext())
                return true;
            // 如果缓存中没有了，调用lookupIterator的方法，此处的lookupIterator即为上一步骤中构造函数初始化的LazyIterator迭代器
            return lookupIterator.hasNext();
        }

        public S next() {
            // 如果有缓存，直接取缓存
            if (knownProviders.hasNext())
                return knownProviders.next().getValue();
            // 如果没有缓存，还是调用lookupIterator中的方法
            return lookupIterator.next();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    };
}
```
可以看到ServiceLoader的load()方法和iterator()方法，最终都是调用了LazyIterator方法，最终具体的加载逻辑都在这个类中，我们继续分析：
```
public final class ServiceLoader<S> implements Iterable<S>
{
    ...
    private class LazyIterator implements Iterator<S>
    {
        public boolean hasNext() {
                // 安全相关，省略...
                return hasNextService(); 
                // 安全相关，省略...
            }
        }

        public S next() {
                // 安全相关，省略...
                return nextService();
                // 安全相关，省略...
            }
        }
    }
}
```
可以看到`LazyIterator`类是ServiceLoader的内部类，hasNext()和next()方法分别调用了hasNextService()和nextService()方法。

我们先来看hasNextServcie()
```
Iterator<String> pending = null;

private boolean hasNextService() {
    if (nextName != null) {
        return true;
    }
    if (configs == null) {
        // 校验配置是否存在，略去
    }
    nextClass = null;
    nextNotFound = false;
    for (;;) {
        while ((pending == null) || !pending.hasNext()) {
            if (!configs.hasMoreElements()) {
                nextName = null;
                return false;
            }
            // 获取配置文件的迭代器
            pending = parse(service, configs.nextElement());
        }
        // 获取下一行值
        nextName = pending.next();

        if (!RestrictedSecurity.isEnabled()) {
            break;
        }
        
        // 省略部分...
        
            // 通过配置文件中配置的类名来加载相应的class对象
            nextClass = Class.forName(nextName, false, loader);
            
        // 省略部分代码...
    }
    return true;
}
```
hasNextService方法主要完成了两件事：
(1) 将配置文件中下一个要加载的实现类名赋值给nextName成员变量
(2) 通过nextName调用Class.forName方法来加载Class对象

再看nextService方法
```
private S nextService() {
    // 先调用上面的hasNextService方法加载下一个需要加载的类名和Class对象
    if (!hasNextService())
        throw new NoSuchElementException();
    // nextName备份一下，再设置成null（以便下次调用hasNextService时会加载下一个）
    String cn = nextName;
    nextName = null;

    // 省略部分代码...
    
    // 获取hasNextService()中设置的nextClass变量，对应下一个要加载的实现类的class对象
    Class<?> c = nextClass;
    if (c == null) {
        // 省略部分代码...
        
        // 如果是null重新加载一次
            c = Class.forName(cn, false, loader);
        // 省略部分代码...
    }
    // 判断实现类是否实现了接口，(service变量是在LazyIterator初始化时传进来的，值为接口对应的Class对象）
    // 如果没有实现接口会报错
    if (!service.isAssignableFrom(c)) {
        fail(service,
                "Provider " + cn  + " not a subtype");
    }
    try {
        // 调用nextInstance()方法生成实例对象，然后强转成service接口类型
        S p = service.cast(c.newInstance());
        // 放入缓存
        providers.put(cn, p);
        // 返回生成的对象
        return p;
    } catch (Throwable x) {
        fail(service,
                "Provider " + cn + " could not be instantiated",
                x);
    }
    throw new Error();          // This cannot happen
}
```
## 1.3 JDK SPI在JDBC中的应用
了解了JDK SPI的实现原理后，我们来看看JDBC中驱动类的加载过程中JDK SPI的应用

JDK中定义了一个java.sql.Driver接口，每种需要支持JDBC的数据库会对这个接口有相应的实现，我们以mysql数据库为例来看。

在使用JDBC连接mysql数据库时，我们会写如下代码：
```
// mysql-connector-java-5.1.6版本后不再需要手动加载
// Class.forName("com.mysql.cj.jdbc.Driver");
Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","123456");
System.out.println("连接成功");
...
```
`DriverManager`是JDK提供的加载JDBC驱动的工具类，在其源码中可以发现如下代码块
```
static {
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","123456");
    System.out.println("连接成功");
}
```
这是个静态代码块，在DriverManager类被加载时会同步执行，我们继续看`loadInitialDrivers`方法的实现细节：
```
private static void loadInitialDrivers() {
    String drivers;
    // 省略部分代码... 以下四行为JDK原注释
    // If the driver is packaged as a Service Provider, load it.
    // Get all the drivers through the classloader
    // exposed as a java.sql.Driver.class service.
    // ServiceLoader.load() replaces the sun.misc.Providers()

    // AccessController.doPrivileged绕过栈检查机制，使得调用者即使没有权限，也可以调用这部分方法（对于JDK的安全机制此处不作更深入讨论）
    AccessController.doPrivileged(new PrivilegedAction<Void>() {
        public Void run() {
            // 初始化ServiceLoader对象，获取Driver接口的实现类
            ServiceLoader<Driver> loadedDrivers = ServiceLoader.load(Driver.class);
            Iterator<Driver> driversIterator = loadedDrivers.iterator();
            try{
                // 依次调用next()方法，使实现类都被实例化出来，
                while(driversIterator.hasNext()) {
                    // 单是此处遍历实例化后的驱动类，但是没有放到registeredDrivers集合中
                    driversIterator.next();
                }
            } catch(Throwable t) {
                // Do nothing
            }
            return null;
        }
    });

    // 省略部分代码...
}
```
而在mysql驱动包中（笔者用的是mysql-connector-java-8.0.29.jar），驱动类com.mysql.cj.jdbc.Driver中也有一个静态代码块：
```
static {
    try {
        DriverManager.registerDriver(new Driver());
    } catch (SQLException var1) {
        throw new RuntimeException("Can't register driver!");
    }
}
```
可以看到此处调用了DriverManager.registerDriver方法，传入了当前Driver类的对象。
```
public static synchronized void registerDriver(java.sql.Driver driver,
            DriverAction da)
        throws SQLException {

    /* Register the driver if it has not already been added to our list */
    if(driver != null) {
        // 将传入的driver对象包装成DriverInfo类，添加到registeredDrivers集合中
        registeredDrivers.addIfAbsent(new DriverInfo(driver, da));
    } else {
        // This is for compatibility with the original DriverManager
        throw new NullPointerException();
    }
    println("registerDriver: " + driver);
}
```
mysql8.2.9 jar包的结构

![](https://files.mdnice.com/user/57877/b2a7b4c6-120b-47f1-a225-5fba3b91343a.png)

mysql5.1.5 jar包的结构

![](https://files.mdnice.com/user/57877/2b79e45e-e4d8-4488-ab4d-3dee3959ae7e.png) 
5.1.6版本开始，jar包中加入了`META-INF/services/java.sql.Driver`文件，DriverManager中的静态代码块调用的`loadInitialDrivers`静态方法中使用SPI机制对配置文件进行了扫描

为什么添加了SPI配置文件，Driver类还需与使用静态代码块来new一个Driver类，调用`DriverManager.registerDriver`方法？

`loadInitialDrivers`方法只会将驱动类都实例化，但是并没有放到`registeredDrivers`集合中，后续创建连接的`getConnection`方法是从`registeredDrivers`方法中获取驱动类实例的，所以需要手动调用这个方法，写在静态代码块中，实例化之前就会执行了。