dubbo没有直接使用JDK SPI，而是借鉴了它的思想

扩展点：用SPI机制查找并加载的接口，也被称为扩展接口，前文介绍的Log和Driver接口都是扩展接口

扩展点实现：实现了扩展接口的具体实现类

JDK SPI在寻找扩展点实现过程中，会加载扩展接口的所有实现类，会将所有实现类实例化

如果我们实现了多个类，但是只需要其中一个类时，就会生成不必要的对象。

例如dubbo框架中的protocol有很多实现类，如果用JDK SPI会加载所有实现类，会造成资源的浪费，

dubbo SPI解决了资源浪费的问题。还对SPI配置文件进行了扩展和修改

首先，按照配置文件的用途把配置文件分成了三类。

META-INF/services/ 目录：该目录下配置文件用来兼容JDK SPI
META-INF/dubbo/目录：该目录用于存放用户自定义SPI配置文件
META-INF/dubbo/internal/ 目录：该目录用于存放dubbo内部使用的SPI配置文件。

dubbo还将SPI文件内容格式改成了key-value格式
如
```
dubbo=org.apache.dubbo.rpc.protocol.dubbo.DubboProtocol
```
key是扩展名，当我们为接口找实现时可以指定一个扩展名来找对应的实现类

还有一个好处是，更容易定位问题，假设没有引用到某个实现类，dubbo SPI抛异常时会携带扩展名信息，而不是实现类无法加载

@SPI注解

改注解修饰的接口类表示该接口是一个扩展接口，可以指定一个默认实现，如格式为 @SPI("dubbo"),默认加载扩展名为dubbo的实现类

dubbo使用ExtensionLoader来加载扩展接口实现，类似JDK SPI中的ServiceLoader

使用方式：
```
Protocol protocol = ExtensionLoader.getExtensionLoader(Protocol.class).getExtention("dubbo");
```
通过扩展名来获取对应的实例。

ExtensionLoader中的三个核心字段

strategies(LoadingStrategy[] 类型)
LoadingStrategy是一个接口，有三个实现，如下图所示
![](https://files.mdnice.com/user/57877/c1f09800-1a87-479f-a492-06c47ce16e02.png)
分别对应前面介绍的三个dubbo SPI配置文件目录。
都继承了Prioritized优先级接口

第二个静态字段是 EXTENTSION_LOADERS，每个扩展接口对应一个ExtensionLoader实例。该集合缓存了全部ExtensionLoader实例。其中key是扩展接口class对象，value是加载的扩展实例对象

第三个静态字段是 EXTENSION_INSTANCE，该map集合用来缓存扩展接口实现类class对象与其实现类的映射关系
```
    private static final ConcurrentMap<Class<?>, ExtensionLoader<?>> EXTENSION_LOADERS = new ConcurrentHashMap<>(64);

    private static final ConcurrentMap<Class<?>, Object> EXTENSION_INSTANCES = new ConcurrentHashMap<>(64);
    
    // 当前ExtentsionLoader实例负责加载的扩展接口class对象
    private final Class<?> type;
    
    // 缓存改接口实现类和扩展名的映射关系
    private final ConcurrentMap<Class<?>, String> cachedNames = new ConcurrentHashMap<>();

    // 缓存名称和实现类class对象的映射关系，上面集合的反向关系缓存
    private final Holder<Map<String, Class<?>>> cachedClasses = new Holder<>();
    
    // 缓存扩展名与扩展实现类对象之间的映射关系
    private final ConcurrentMap<String, Holder<Object>> cachedInstances = new ConcurrentHashMap<>();


    // 配置扩展接口上面的@SPI注解的value值，也就是默认扩展名
    private String cachedDefaultName;

```
`getExtensionLoader`方法，通过接口的Class对象从EXTENSION_LOADERS缓存中获取ExtensionLoader
```
public static <T> ExtensionLoader<T> getExtensionLoader(Class<T> type) {
    if (type == null) {
        throw new IllegalArgumentException("Extension type == null");
    }
    if (!type.isInterface()) {
        throw new IllegalArgumentException("Extension type (" + type + ") is not an interface!");
    }
    if (!withExtensionAnnotation(type)) {
        throw new IllegalArgumentException("Extension type (" + type +
                ") is not an extension, because it is NOT annotated with @" + SPI.class.getSimpleName() + "!");
    }

    ExtensionLoader<T> loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    if (loader == null) {
        EXTENSION_LOADERS.putIfAbsent(type, new ExtensionLoader<T>(type));
        loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    }
    return loader;
}
```
获取ExtensionLoader后，会调用getExtension方法来获取实现类的实例
```
public T getExtension(String name) {
    if (StringUtils.isEmpty(name)) {
        throw new IllegalArgumentException("Extension name == null");
    }
    if ("true".equals(name)) {
        return getDefaultExtension();
    }
    final Holder<Object> holder = getOrCreateHolder(name);
    Object instance = holder.get();
    if (instance == null) {
        // double check防止并发问题
        synchronized (holder) {
            instance = holder.get();
            if (instance == null) {
                // 根据扩展名获取相应的SPI实现类
                instance = createExtension(name);
                holder.set(instance);
            }
        }
    }
    return (T) instance;
}
```
createExtension方法中完成了SPI实现类的查找和实例化,和自动装配，自动wrapper包装等功能
```
private T createExtension(String name) {
    // 通过扩展名从cachedClasses缓存中获取实现类class对象，如果cachedClasses未初始化，会扫描前面介绍的三种目录来加载实现类，下文有更详细解释
    Class<?> clazz = getExtensionClasses().get(name);
    if (clazz == null) {
        throw findException(name);
    }
    try {
        // 从缓存中获取实例化的对象，如果没有实例化过，就通过反射来重新创建一个
        T instance = (T) EXTENSION_INSTANCES.get(clazz);
        if (instance == null) {
            EXTENSION_INSTANCES.putIfAbsent(clazz, clazz.newInstance());
            instance = (T) EXTENSION_INSTANCES.get(clazz);
        }
        // 自动装配，会调用setter方法，涉及到ExtensionFacotry和自动装配的相关内容，后面会详细介绍。
        injectExtension(instance);
        // 自动包装，Wrapper相关，后面会详细介绍 
        Set<Class<?>> wrapperClasses = cachedWrapperClasses;
        if (CollectionUtils.isNotEmpty(wrapperClasses)) {
            for (Class<?> wrapperClass : wrapperClasses) {
                instance = injectExtension((T) wrapperClass.getConstructor(type).newInstance(instance));
            }
        }
        // 如果实现类实现了Lifecycle方法，这里会调用实现类的initialize()方法
        
        
        initExtension(instance);
        return instance;
    } catch (Throwable t) {
        throw new IllegalStateException("Extension instance (name: " + name + ", class: " +
                type + ") couldn't be instantiated: " + t.getMessage(), t);
    }
}
```
深入`getExtensionClasses`方法来看class文件的加载细节
```
private Map<String, Class<?>> getExtensionClasses() {
    Map<String, Class<?>> classes = cachedClasses.get();
    if (classes == null) {
        synchronized (cachedClasses) {
            classes = cachedClasses.get();
            if (classes == null) {
                classes = loadExtensionClasses();
                cachedClasses.set(classes);
            }
        }
    }
    return classes;
}

private Map<String, Class<?>> loadExtensionClasses() {
    cacheDefaultExtensionName();

    Map<String, Class<?>> extensionClasses = new HashMap<>();

    for (LoadingStrategy strategy : strategies) {
        loadDirectory(extensionClasses, strategy.directory(), type.getName(), strategy.preferExtensionClassLoader(), strategy.overridden(), strategy.excludedPackages());
        loadDirectory(extensionClasses, strategy.directory(), type.getName().replace("org.apache", "com.alibaba"), strategy.preferExtensionClassLoader(), strategy.overridden(), strategloadLoadingStrategiesy.excludedPackages());
    }

    return extensionClasses;
}
```
`strategies`这个变量比较有意思，上文已经分析过，这个成员变量里存放的是对应三种配置文件的加载策略，我们来看它的初始化过程`loadLoadingStrategies()`方法
```
private static volatile LoadingStrategy[] strategies = loadLoadingStrategies();
```

```
private static LoadingStrategy[] loadLoadingStrategies() {
    return stream(load(LoadingStrategy.class).spliterator(), false)
            .sorted()
            .toArray(LoadingStrategy[]::new);
}
```
可以看到核心方法是`load(LoadingStrategy.class)`方法，而这个方法对应的其实就是JDK中的ServiceLoader中的load方法，也就是说，这里加载strategies集合用的是JDK SPI的方法

可以在`dubbo-common`模块中的`resources/META-INF/services/`目录下看到相应的配置文件

![](https://files.mdnice.com/user/57877/971639c8-4e16-49c2-9a71-b58a94efbfac.png)

# @Adaptive注解以及适配器内容

![](https://files.mdnice.com/user/57877/35610cbd-4e61-4b61-af6a-c80a590343b5.png)

@Adaptive注解用来实现dubbo的适配器功能 ,上图中ExtensionFactory的实现类中的`AdaptiveExtensionFactory`的作用其实是为了在不同场景下决定使用`SpiExtensionFactory`还是`SpringExtentsionFactory`

@Adaptive注解还可以修饰接口的方法，如org.apache.dubbo.remoting.Transporter接口中有两个@Adaptive修饰的方法bind和connect，dubbo动态生成的适配器类如下：

![](https://files.mdnice.com/user/57877/472fff80-1a9f-4e72-9580-1e17ac8f7474.png)


![](https://files.mdnice.com/user/57877/bbea3e74-db5a-46a5-bec5-e56d98a2d195.png)
bind方法类似，不再展示

具体生成逻辑在`ExtensionLoader.createAdaptiveExtensionClass`方法中，该方法中首先调用`AdaptiveClassCodeGenerator.generate()`方法生成实现类的java代码，生成字符串的形式，然后调用dubbo中的编译器org.apache.dubbo.common.compiler.Compiler类的`compile`方法，入参是字符串形式的java代码和类加载器，返回值是一个Class对象。
```
private Class<?> createAdaptiveExtensionClass() {
    String code = new AdaptiveClassCodeGenerator(type, cachedDefaultName).generate();
    ClassLoader classLoader = findClassLoader();
    org.apache.dubbo.common.compiler.Compiler compiler = ExtensionLoader.getExtensionLoader(org.apache.dubbo.common.compiler.Compiler.class).getAdaptiveExtension();
    return compiler.compile(code, classLoader);
}
```
回到createExtension方法，该方法最终调用了ExtensionLoader.loadClass方法，这个方法对Adaptive注解进行了处理，代码如下：

![](https://files.mdnice.com/user/57877/00910c72-6786-4f60-bbdb-f0508e19309d.png)

```
private void loadClass(Map<String, Class<?>> extensionClasses, java.net.URL resourceURL, Class<?> clazz, String name,
                       boolean overridden) throws NoSuchMethodException {
    // 判断是否有继承关系，没有就报错
    if (!type.isAssignableFrom(clazz)) {
        throw new IllegalStateException("Error occurred when loading extension class (interface: " +
                type + ", class line: " + clazz.getName() + "), class "
                + clazz.getName() + " is not subtype of interface.");
    }
    // 类上是否有Adaptive注解
    if (clazz.isAnnotationPresent(Adaptive.class)) {
        // 有Adaptive注解，调用这个方法，没有就调用下面的方法，方法中会给成员变量cachedAdaptiveClass赋值
        cacheAdaptiveClass(clazz, overridden);
    } else if (isWrapperClass(clazz)) {
        cacheWrapperClass(clazz);
    } else {
        clazz.getConstructor();
        if (StringUtils.isEmpty(name)) {
            name = findAnnotationName(clazz);
            if (name.length() == 0) {
                throw new IllegalStateException("No such extension name for the class " + clazz.getName() + " in the config " + resourceURL);
            }
        }

        String[] names = NAME_SEPARATOR.split(name);
        if (ArrayUtils.isNotEmpty(names)) {
            cacheActivateClass(clazz, names[0]);
            for (String n : names) {
                cacheName(clazz, n);
                saveInExtensionClass(extensionClasses, clazz, n, overridden);
            }
        }
    }
}
```
通过API方式，(addExtension()方法) 设置cachedAdaptiveClass字段，指定适配器类型

一个扩展接口可能有多个实现类，这些实现类可能有些相同的逻辑，Wrapper类就是为了解决不同实现类之间代码复用问题，可以理解成装饰器，Wrapper类也实现了扩展接口

ExtensionLoader.loadClass方法中有个方法`isWrapperClass(clazz)`判断是否是Wrapper类，判断的方法是看类中是否有拷贝构造函数（即构造函数只有一个参数且为扩展接口类型）。如果判断结果为Wrapper类，会调用`cacheWrapperClass(clazz)`将Wrapper类缓存起来

后面在ExtensionLoader.createExtension方法中会遍历`cachedWrapperClasses`缓存集合中的全部Wrapper类，并一层层包装到扩展实现对象外层
```
 for (Class<?> wrapperClass : wrapperClasses) {
     instance = injectExtension((T) wrapperClass.getConstructor(type).newInstance(instance));
 }
```
包装过程是调用`injectExtension`方法，方法中会实例化一个Wrapper类，该类构造函数传参为当前扩展实现类，在`injectExtension`方法中会扫描扩展接口的全部setter方法，在真正的扩展实现类对象中调用相应的settter方法，相当于将Wrapper类中的setter方法全部应用到了扩展实现类对象中。（不包含简单类型属性对于的setter方法）
```
private T injectExtension(T instance) {

    if (objectFactory == null) {
        return instance;
    }
    try {
        for (Method method : instance.getClass().getMethods()) {
            if (!isSetter(method)) {
                continue;
            }
            /**
             * Check {@link DisableInject} to see if we need auto injection for this property
             */
            if (method.getAnnotation(DisableInject.class) != null) {
                continue;
            }
            Class<?> pt = method.getParameterTypes()[0];
            if (ReflectUtils.isPrimitives(pt)) {
                // 如果参数为简单类型，忽略setter方法
                continue;
            }

            try {
                String property = getSetterProperty(method);
                Object object = objectFactory.getExtension(pt, property);
                if (object != null) {
                    method.invoke(instance, object);
                }
            } catch (Exception e) {
                logger.error("Failed to inject via method " + method.getName()
                        + " of interface " + type.getName() + ": " + e.getMessage(), e);
            }

        }
    } catch (Exception e) {
        logger.error(e.getMessage(), e);
    }
    return instance;
}
```
下面我们来看ExtensionFactory.getExtension的实现逻辑。
SpiExtensionFactory:
```
public class SpiExtensionFactory implements ExtensionFactory {

    @Override
    public <T> T getExtension(Class<T> type, String name) {
        if (type.isInterface() && type.isAnnotationPresent(SPI.class)) {
            // 通过dubbo SPI机制加载ExtensionLoader实例
            ExtensionLoader<T> loader = ExtensionLoader.getExtensionLoader(type);
            if (!loader.getSupportedExtensions().isEmpty()) {
                return loader.getAdaptiveExtension();
            }
        }
        return null;
    }

}
```
SpringExtensionFactory:
```
public <T> T getExtension(Class<T> type, String name) {

    //SPI should be get from SpiExtensionFactory
    if (type.isInterface() && type.isAnnotationPresent(SPI.class)) {
        return null;
    }
    // 从spring容器中获取
    for (ApplicationContext context : CONTEXTS) {
        T bean = BeanFactoryUtils.getOptionalBean(context, name, type);
        if (bean != null) {
            return bean;
        }
    }

    logger.warn("No spring extension (bean) named:" + name + ", try to find an extension (bean) of type " + type.getName());

    return null;
}
```

## @Activate注解与自动计划属性
Filter接口有很多实现类，每个场景需要的实现类不一样，需要一套配置来指定当前场景中哪些实现类是可用的，这就是Activate注解的作用。标注在扩展实现类上
 
![](https://files.mdnice.com/user/57877/bd37af9e-0a7d-402d-b04a-557b2f51731d.png)

`ExtensionLoader.loadClass`方法中对这个注解进行了处理，
```
if (clazz.isAnnotationPresent(Adaptive.class)) {
     cacheAdaptiveClass(clazz, overridden);
}
...
cacheActivateClass(clazz, names[0]);
```
如果有这个注解，首先会缓存到`cachedAdaptiveClass`成员变量中，随后，当

会将扩展实现类名称name和Activate注解对象缓存到cachedActivates集合中

`public List<T> getActivateExtension(URL url, String[] values, String group)`方法中使用了cachedActivates集合,集合入参中有url，values和group（可选值是provider和consumer）

首先获取默认激活的扩展实现类集合，有4个条件：
- 1、在cachedActivities集合中存在
- 2、Activity注解指定的group与当前group匹配
- 3、扩展名没有出现在values中，未在配置中明确指定，也未明确删除
- 4、url中出现了Activity注解中指定的key

接下来忽略判断-开头的配置明确指定不激活的实现类

再按照顺序将自定义扩展器添加到默认扩展后面。


